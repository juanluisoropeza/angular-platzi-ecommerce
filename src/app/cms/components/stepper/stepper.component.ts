import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => StepperComponent),
      multi: true,
    },
  ],
})
export class StepperComponent implements ControlValueAccessor {
  currentValue = 0;
  onChange: any = () => {};
  onTouch: any = () => {};
  isDesabled = false;

  add() {
    this.currentValue++;
    this.onTouch();
    this.onChange(this.currentValue);
  }

  sub() {
    this.currentValue--;
    this.onTouch();
    this.onChange(this.currentValue);
  }

  // se utiliza para desde el form reactivo poner un valor por defecto
  writeValue(value: number): void {
    if (value) {
      this.currentValue = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDesabled = isDisabled;
  }
}
