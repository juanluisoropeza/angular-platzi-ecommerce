import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MyValidators } from '../../utils/validators';

@Component({
  selector: 'app-basic-forms',
  templateUrl: './basic-forms.component.html',
  styleUrls: ['./basic-forms.component.css'],
})
export class BasicFormsComponent implements OnInit {
  form!: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.nameField?.valueChanges.subscribe((value) => {
      console.log(value);
    });
    // para escuchar todos los cambios en el form
    this.form.valueChanges.subscribe((value) => {
      console.log(value);
    });
  }

  private buildForm() {
    this.form = this.formBuilder.group(
      {
        fullName: this.formBuilder.group({
          name: [
            '',
            [
              Validators.required,
              Validators.maxLength(10),
              Validators.pattern(/^[a-zA-Z ]+$/),
            ],
          ],
          last: [
            '',
            [
              Validators.required,
              Validators.maxLength(20),
              Validators.pattern(/^[a-zA-Z ]+$/),
            ],
          ],
        }),
        email: ['', [Validators.required, Validators.email]],
        phone: ['', Validators.required],
        color: ['#000000'],
        date: [''],
        age: [
          18,
          [Validators.required, Validators.min(18), Validators.max(100)],
        ],
        category: ['category-1'],
        tag: [''],
        agree: [false, Validators.requiredTrue],
        gender: [''],
        zone: [''],
        minRange: ['', [Validators.required]],
        maxRange: ['', [Validators.required]],
        address: this.formBuilder.array([]),
        stock: [5, [Validators.required]],
      },
      {
        validators: MyValidators.range(0, 100),
      }
    );

    this.form.get('stock')?.valueChanges.subscribe((value) => {
      console.log(value);
    });
  }

  addAddressField() {
    this.addressField.push(this.createAddressField());
  }

  private createAddressField() {
    return this.formBuilder.group({
      zip: ['', Validators.required],
      text: ['', Validators.required],
    });
  }

  getNameValue() {
    console.log(this.nameField?.value);
  }

  get addressField() {
    return this.form.get('address') as FormArray;
  }

  get nameField() {
    return this.form.get('fullName')?.get('name');
  }

  get lastField() {
    return this.form.get('fullName.last');
  }

  get emailField() {
    return this.form.get('email');
  }

  get phoneField() {
    return this.form.get('phone');
  }

  get colorField() {
    return this.form.get('color');
  }

  get dateField() {
    return this.form.get('date');
  }

  get ageField() {
    return this.form.get('age');
  }

  get categoryField() {
    return this.form.get('category');
  }

  get tagField() {
    return this.form.get('tag');
  }

  get agreeField() {
    return this.form.get('agree');
  }

  get genderField() {
    return this.form.get('gender');
  }

  get zoneField() {
    return this.form.get('zone');
  }

  get isNameFieldValid() {
    return this.nameField?.touched && this.nameField.valid;
  }

  get isNameFieldInvalid() {
    return this.nameField?.touched && this.nameField.invalid;
  }

  get minField() {
    return this.form.get('minRange');
  }

  get maxField() {
    return this.form.get('maxRange');
  }

  save(event: any) {
    if (this.form.valid) {
      console.log(this.form.value);
    } else {
      this.form.markAllAsTouched();
    }
  }
}
