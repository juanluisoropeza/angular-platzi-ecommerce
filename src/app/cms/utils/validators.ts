import { AbstractControl } from '@angular/forms';
import { map } from 'rxjs/operators';
import { CategoriesService } from '../modules/categories/services/categories.service';

export class MyValidators {
  static validatePassword(control: AbstractControl) {
    const value = control.value;
    if (!containsNumber(value)) {
      return { invalid_password: true };
    }
    return null;
  }

  static matchPasswords(control: AbstractControl) {
    const passwordControl = control.get('password');
    const confirmPasswordControl = control.get('confirmPassword');

    if (passwordControl && confirmPasswordControl) {
      const password = passwordControl.value;
      const confirmPassword = confirmPasswordControl.value;

      if (password !== confirmPassword) {
        return { match_password: true };
      }
    }
    return null;
  }

  static range(minRange: number, maxRange: number) {
    return (control: AbstractControl) => {
      const min = control.get('min')?.value;
      const max = control.get('max')?.value;

      if (min === '' && max === '') {
        return null;
      }

      if (min <= minRange || max >= maxRange) {
        return { range: true };
      }

      return null;
    };
  }

  static validateCategory(service: CategoriesService) {
    return (control: AbstractControl) => {
      const value = control.value;
      return service.checkCategory(value).pipe(
        map((response) => {
          const isAvailable = response.isAvailable;
          if (!isAvailable) {
            return { not_available: true };
          }
          return null;
        })
      );
    };
  }
}

const containsNumber = (value: string) => {
  return value.split('').find((v) => isNumber(v)) !== undefined;
};

const isNumber = (value: string): boolean => {
  return !isNaN(parseInt(value, 10));
};
