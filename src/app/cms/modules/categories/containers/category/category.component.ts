import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Category } from '../../models/category.model';
import { CategoriesService } from '../../services/categories.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
})
export class CategoryComponent implements OnInit {
  category!: Category;

  constructor(
    private categoriesService: CategoriesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      if (Number(params['id'])) {
        this.getCategory(Number(params['id']));
      }
    });
  }

  createCategory(data: any) {
    this.categoriesService.createCategory(data).subscribe((resp) => {
      console.log(resp);
      this.router.navigate(['./cms/categories']);
    });
  }

  updateCategory(data: any) {
    if (this.category?.id) {
      this.categoriesService
        .updateCategory(this.category?.id, data)
        .subscribe((resp) => {
          this.router.navigate(['./cms/categories']);
        });
    }
  }

  private getCategory(id: number) {
    if (id) {
      this.categoriesService.getCategory(id).subscribe((resp) => {
        this.category = resp;
        /*
          tambien se puede hacer valor por valor
          this.nameField.setValue(resp.name);
        */
      });
    }
  }
}
