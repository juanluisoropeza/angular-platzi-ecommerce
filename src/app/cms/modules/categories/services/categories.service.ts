import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  constructor(private http: HttpClient) {}

  getAllCategories() {
    return this.http.get<Category[]>(`${environment.cmsApiUrl}/categories`);
  }

  getCategory(id: number) {
    return this.http.get<Category>(`${environment.cmsApiUrl}/categories/${id}`);
  }

  createCategory(data: Partial<Category>) {
    return this.http.post<Category>(
      `${environment.cmsApiUrl}/categories`,
      data
    );
  }

  updateCategory(id: number, data: Partial<Category>) {
    return this.http.put<Category>(
      `${environment.cmsApiUrl}/categories/${id}`,
      data
    );
  }

  deleteCategory(id: number) {
    return this.http.delete<Category>(
      `${environment.cmsApiUrl}/categories/${id}`
    );
  }

  checkCategory(name: string) {
    return this.http.post<{ isAvailable: boolean }>(
      `${environment.cmsApiUrl}/categories/availability`,
      {
        name,
      }
    );
  }
}
