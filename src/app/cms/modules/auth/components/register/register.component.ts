import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../../../services/auth.service';
import { MyValidators } from '../../../../utils/validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  form!: FormGroup;
  showCompany = true;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
    this.buildForm();
  }

  private buildForm() {
    this.form = this.formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            MyValidators.validatePassword,
          ],
        ],
        confirmPassword: ['', [Validators.required]],
        type: ['company', [Validators.required]],
        companyName: ['', [Validators.required]],
      },
      {
        validators: MyValidators.matchPasswords,
      }
    );
    this.form.get('type')?.valueChanges.subscribe((value) => {
      if (value === 'company') {
        this.form.get('companyName')?.setValidators([Validators.required]);
        this.showCompany = true;
      } else {
        this.form.get('companyName')?.setValidators(null);
        this.showCompany = false;
      }
      this.companyFieldField?.updateValueAndValidity();
    });
  }

  register(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      console.log(value);
      // crear usuario con el servicio
    }
  }

  get typeField() {
    return this.form.get('type');
  }

  get companyFieldField() {
    return this.form.get('companyName');
  }
}
