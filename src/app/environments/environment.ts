export const environment = {
  production: false,
  apiUrl: 'https://damp-spire-59848.herokuapp.com',
  cmsApiUrl: 'https://api.escuelajs.co/api/v1',
};
