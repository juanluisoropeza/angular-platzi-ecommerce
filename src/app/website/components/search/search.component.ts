import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  searchField = new FormControl();
  results: any[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.searchField.valueChanges.pipe(debounceTime(500)).subscribe((value) => {
      this.getData(value);
    });
  }

  private getData(query: string) {
    const API = '2Z3HYUiJa30OYeMrrAY3NivQeeGfoA2Z';
    this.http
      .get(
        `https://api.giphy.com/v1/gifs/search?q=${query}&api_key=${API}&limit=20`
      )
      .pipe(
        map((response: any) => {
          return response.data.map((item: any) => item.images.downsized.url);
        })
      )
      .subscribe((data) => {
        console.log(data);
        this.results = data;
      });
  }
}
